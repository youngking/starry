#!/usr/bin/env python
# -*- coding: utf-8 -*-

from protorpc import messages
from protorpc import remote

from interface import  (SimpleRequest,
                         SimpleResponse,
                         BasicService,
                        )

def client(name='client', address='tcp://0.0.0.0:5000', service_name='basic_service'):
    from starry.transport  import TcpTransport
    basic_service = BasicService.Stub(TcpTransport(name, address, service_name))
    return basic_service

if __name__ == '__main__':
    basic_service = client()
    print basic_service.transport.ping()
    request = SimpleRequest()
    request.param1 = 'hello, '
    import time
    t1 = time.time()
    print " --------------  sync test  ----------------"
    for i in range(10):
        print i
        request.param2 = 'world %s'%i
        print basic_service.test(request)
    t2 = time.time()
    print "tested: ", t2 -t1
