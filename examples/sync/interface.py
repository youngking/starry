#!/usr/bin/env python
# -*- coding: utf-8 -*-

from protorpc import messages
from protorpc import remote

class SimpleRequest(messages.Message):
  """Simple request message type used for tests."""

  param1 = messages.StringField(1)
  param2 = messages.StringField(2)


class SimpleResponse(messages.Message):
  """Simple response message type used for tests."""
  value = messages.StringField(1)


class BasicService(remote.Service):
  """A basic service with decorated remote method."""

  def __init__(self):
    self.request_ids = []

  @remote.method(SimpleRequest, SimpleResponse)
  def test(self, request):
      raise NotImplementedError('Method test is not implemented')
