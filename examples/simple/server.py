#!/usr/bin/env python
# -*- coding: utf-8 -*-

from protorpc import messages
from protorpc import remote
import time
from starry.utils import options

from interface import  (SimpleRequest,
                         SimpleResponse,
                         BasicService,
                        )

class BasicServiceImpl(BasicService):

    def test(self, request):
        time.sleep(0.1)
        return SimpleResponse(value = request.param1+request.param2)


def create_server(address='tcp://0.0.0.0:5000', service_name='basic_service'):
    from starry.server import RPCServer
    rpcserver = RPCServer('rpcserver', address)
    rpcserver.register_service(BasicServiceImpl(), service_name)
    return rpcserver


if __name__ == '__main__':
    server = create_server()
    server.start()
