#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup,find_packages
import os
import sys

def read_file(*path):
    base_dir = os.path.dirname(__file__)
    file_path = (base_dir, ) + tuple(path)
    return file(os.path.join(*file_path)).read()

install_requires = ['pyzmq-static',
                    'pyyaml',
                    'protorpc==0.4']

setup(
    name = "starry",
    url = "http://www.zhihu.com",
    license="Private",
    author = "Young King,Rio",
    author_email = "dev@zhihu.com",
    description = "",
    long_description = (
        read_file("README.rst") + "\n\n" +
        "Change History\n" +
        "==============\n\n" +
        read_file("CHANGES.rst")),
    version = "0.22",
    packages = find_packages('.'),
    package_dir = {'': '.'},
    include_package_data = True, 
    zip_safe=False,
    test_suite='nose.collector',
    tests_require=['Nose','mox'],
    install_requires=install_requires,
    dependency_links = [
        'https://bitbucket.org/youngking/protorpc/get/0.4.zip#egg=protorpc-0.4',
    ],
    classifiers = [
        "Programming Language :: Python",
        "Operating System :: OS Independent",
        "License :: OSI Approved :: BSD License",
        "Topic :: Software Development :: Libraries",
        "Topic :: Utilities",
    ],
)
