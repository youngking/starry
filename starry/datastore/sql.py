#!/usr/bin/env python
# -*- coding: utf-8 -*-
from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy import Table
from sqlalchemy import MetaData
from sqlalchemy import asc,desc

from starry.datastore import get_store_config
from starry.globals import g

def connect(name='default'):
    """
    Get a connection from the data store given by argument ``name``.

    This will return the  prepared ``Connection`` object when there is one,
    or create one.

    The result ``Connection`` object will be closed automaticlly when
    the context is finished. Such a close operation does not actually
    close the DBAPI connection, but instead returns it to the connection
    pool referenced by the engine.

    :param name: the data store name
    :type name: str
    :param newly: should or should not return a newly ``Connection`` object
    :type newly: bool
    :rtype: sqlalchemy.engine.Connection
    """
    name = getattr(g, '__starry_dbname', None) or name
    config = get_store_config('sqlStore').get(name)
    db_uri = "{engine}://{username}:{password}@{host}/{database}?charset=utf8".format(**config)
    attr = '_sql_{0}'.format(name)
    if not hasattr(g, attr):
        engine = create_engine(db_uri,
                                strategy='threadlocal',
                                encoding='utf-8',
                                max_overflow=2,
                                pool_recycle=3600,
                            )
        setattr(g, attr, engine)
    return getattr(g, attr)

@contextmanager
def chosen_db(name):
    """
    >>> with chosen_db('master') as db:
    ...     member_dao.get_member(4)
    >>> with chosen_db('slave2'):
    ...     member_dao.get_member(4)
    """

    try:
        g.__starry_name = name
        yield connect()
    finally:
        g.__starry_name = None


def build_order(**kwargs):
    """
    根据参数中的 order_col 和 order_dir 构建 ORDER BY 子句。
    """
    order_col = kwargs.get('order_col', 'id')
    order_dir = kwargs.get('order_dir', 'DESC')
    if order_dir == 'DESC':
        return desc(order_col)
    else:
        return asc(order_col)



class Tables(object):

    _tables = {}
    meta = MetaData()

    def __getattr__(self, name):
        cls = Tables
        if name not in cls._tables:
            cls._tables[name] = Table(name,cls.meta,autoload=True,autoload_with=connect())
        return cls._tables[name]

table = Tables()
