#!/usr/bin/env python
# -*- coding: utf-8 -*-
from starry.utils.options import options

def get_store_config(name=None):
    return getattr(options, name)
