#!/usr/bin/env python
# -*- coding: utf-8 -*-
from redis_shard.shard import RedisShardAPI

from starry.datastore import get_store_config
from starry.globals import g

def connect(name='default'):
    """
    Get a connection from the data store given by argument ``name``.

    If the argument ``newly`` is ``True``, this will return a newly
    allocated ``Connection`` object. Otherwise this will return the
    prepared ``Connection`` object when there is one, or create one.
    The result ``Connection`` object will be closed automaticlly when
    the context is finished. Such a close operation does not actually
    close the DBAPI connection, but instead returns it to the connection
    pool referenced by the engine.

    :param name: the data store name
    :type name: str
    :param newly: should or should not return a newly ``Connection`` object
    :type newly: bool
    :rtype: sqlalchemy.engine.Connection
    """
    config = get_store_config('redisclusterStore').get(name)
    attr = '_rediscluster_{0}'.format(name)
    if not hasattr(g, attr):
        client = RedisShardAPI(config)
        setattr(g, attr, client)
    return getattr(g, attr)
