#!/usr/bin/env python
# -*- coding: utf-8 -*-
from protorpc import messages
from protorpc import remote

package = 'starry'

class Request(messages.Message):
    client_id  = messages.StringField(1, required=True)
    service_name  = messages.StringField(2, required=True)
    method_name  = messages.StringField(3, required=True)
    request = messages.BytesField(4, required=True)

class Response(messages.Message):
    response = messages.BytesField(1)
    status = messages.MessageField('remote.RpcStatus', 2)
