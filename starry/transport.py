#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import threading
import zmq

from protorpc.transport import Transport
from protorpc.transport import Rpc
from protorpc import protobuf
from protorpc import remote

from .rpc_pb import Request
from .rpc_pb import Response

logger = logging.getLogger('starry.transport')


class ZmqRequest(object):

    ctxs = {}

    def __init__(self, transport, service_name, encoded_request):
        self._transport = transport
        cls = ZmqRequest
        t = threading.currentThread()
        contexts = cls.ctxs.setdefault(service_name, {})
        if t in contexts:
            tctx = contexts[t]
            ctx = tctx['ctx']
            socket = tctx['socket']
        else:
            # create new context for thread
            ctx = zmq.Context()
            socket = False
            tctx = {'ctx': ctx, 'socket': socket}
            contexts[t] = tctx
            # clean up contexts for threads that are not running anymore
            for thr in contexts.keys():
                if not thr.isAlive():
                    try:
                        del contexts[thr]
                    except:
                        pass
        if not socket or socket.closed:
            socket = ctx.socket(zmq.REQ)
            tctx['socket'] = socket
            socket.connect(self._transport.address)
        self.__service_name = service_name
        self.socket = socket
        self.poller = zmq.Poller()
        self.poller.register(self.socket, zmq.POLLIN | zmq.POLLOUT)

        self._start_request(encoded_request)

        # inlitlize zmq

    def _start_request(self, encoded_request):
        socks = dict(self.poller.poll(self._transport.timeout \
                * 1000))  # this shouldn't be timeout
        assert self.socket in socks
        assert socks[self.socket] == zmq.POLLOUT
        self.socket.send(encoded_request)

    def get_response(self):
        try:
            # receiving
            socks = dict(self.poller.poll(self._transport.timeout * 1000))
            assert self.socket in socks
            assert socks[self.socket] == zmq.POLLIN
            reply = self.socket.recv(zmq.NOBLOCK)
            resp = self._transport.protocol.decode_message(Response, reply)
            if resp.response:
                return resp.response, None
            else:
                return None, resp.status

        except AssertionError as e:
            self.socket.setsockopt(zmq.LINGER, 0)
            self.socket.close()
            return None, remote.RpcStatus(
                    state=remote.RpcState.NETWORK_ERROR,
                    error_message='Network Error ({0}) : {1}'.format(\
                            self.__service_name, e.message))
        except Exception as e:
            return None, remote.RpcStatus(
                    state=remote.RpcState.SERVER_ERROR,
                    error_message='Server Error ({0}) : {1}'.format(\
                            self.__service_name, e.message))


class TcpTransport(Transport):

    def __init__(self, client_id, address, service_name,\
            timeout=1, protocol=protobuf):
        super(TcpTransport, self).__init__(protocol=protocol)
        self.client_id = client_id
        self.address = address
        self.__service_name = service_name
        self.timeout = timeout
        self.__request_type = ZmqRequest

    def _start_rpc(self, remote_info, request):
        """Start a remote procedure call.

        Args:
            remote_info: A RemoteInfo instance for this RPC.
            request: The request message for this RPC.

        Returns:
            An Rpc instance initialized with a Request.
        """
        method_name = remote_info.method.func_name
        response_type = remote_info.response_type
        return self.call_remote(method_name, request, response_type)

    def ping(self):
        req = Request()
        req.client_id = self.client_id
        req.service_name = self.__service_name
        req.method_name = 'ping'
        req.request = ''
        tcp_request = self.__request_type(
                            transport=self,
                            service_name=self.__service_name,
                            encoded_request=self.protocol.encode_message(req),
                            )

        encoded_response, status = tcp_request.get_response()
        return encoded_response

    def call_remote(self, method_name, request, response_type):
        """Start a remote procedure call.

        Args:
            method_name: called method for this RPC.
            request: The request message for this RPC.

        Returns:
            An Rpc instance initialized with a Request.
        """
        req = Request()
        req.client_id = self.client_id
        req.service_name = self.__service_name
        req.method_name = method_name
        req.request = self.protocol.encode_message(request)
        tcp_request = self.__request_type(
                            transport=self,
                            service_name=self.__service_name,
                            encoded_request=self.protocol.encode_message(req),
                            )
        rpc = Rpc(request)

        def wait_impl():
            encoded_response, status = tcp_request.get_response()
            if status:
                rpc.set_status(status)
            else:
                response = self.protocol.decode_message(response_type,
                                                        encoded_response
                                                    )
                rpc.set_response(response)

        rpc._wait_impl = wait_impl

        return rpc
