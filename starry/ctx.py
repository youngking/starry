#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .globals import _request_ctx_stack

class _RequestGlobals(object):
    pass

class RequestContext(object):
    """The request context contains all request relevant information.  It is
    created at the beginning of the request and pushed to the
    `_request_ctx_stack` and removed at the end of it. 
    """
    def __init__(self, app):
        self.app = app
        self.g = _RequestGlobals()

    def push(self):
        """Binds the request context to the current context."""
        _request_ctx_stack.push(self)

    def pop(self):
        """Pops the request context and unbinds it by doing that.  This will
        also trigger the execution of functions registered by the
        :meth:`~flask.Flask.teardown_request` decorator.
        """
        _request_ctx_stack.pop()

    def __enter__(self):
        self.push()
        return self

    def __exit__(self, exc_type, exc_value, tb):
        # do not pop the request stack if we are in debug mode and an
        # exception happened.  This will allow the debugger to still
        # access the request object in the interactive shell.  
        if tb is None:
            self.pop()
