#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import yaml
import os

from starry.utils import options

from .ctx import RequestContext

class Application(object):
        
    running = 0

    def __init__(self, root_path):
        self.servers = []
        self.named_servers = {}
        self.root_path = root_path

    def load_config(self,config_file, parse_command= True):
        file_path = os.path.join(self.root_path,config_file)
        options.parse_config_file(file_path)
        if parse_command:
            options.parse_command_line()
        self.settings = options.options

    def request_context(self):
        return RequestContext(self)

    def run(self):
        self.start()

    def add_server(self, server):
        """
        Args:
           server: A Server instance.
        """
        channel = server.channel or server.__class__.__name__
        if self.named_servers.has_key(channel):
            raise RuntimeError("cannot have two servers with same name"
                                " '%s'" % channel)
        self.named_servers[channel] = server
        server.app = self
        self.servers.append(server)
        if self.running:
            server.start()

    def remove_server(self, server):
        channel = server.channel or server.__class__.__name__
        server.app = None
        del self.named_servers[channel]
        self.servers.remove(server)
        if self.running:
            server.stop()

    def get_server(self, channel):
        return self.named_servers[channel]

    def start(self):
        #  TODO 异步处理
        self.running = 1
        for server in self.servers:
            server.start()

    def stop(self):
        #  TODO 异步处理
        self.running = 0
        for server in self.servers:
            server.stop()
