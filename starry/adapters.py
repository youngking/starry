#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .ctx import RequestContext

class ServerAdapter(object):
    quiet = False
    def __init__(self, host='127.0.0.1', port=8080, **config):
        self.options = config
        self.host = host
        self.port = int(port)

    def run(self, handler): # pragma: no cover
        pass

    def __repr__(self):
        args = ', '.join(['%s=%s'%(k,repr(v)) for k, v in self.options.items()])
        return "%s(%s)" % (self.__class__.__name__, args)

class TornadoServer(object):
    def __init__(self, settings, handlers):
        self.channel = settings.pop('channel')
        self.settings = settings
        self.handlers = handlers

    def start(self):
        import tornado.web
        app = tornado.web.Application(self.handlers, **self.settings)
        app.request_context = lambda :RequestContext(app) 
        http_server = tornado.httpserver.HTTPServer(app, xheaders = True)
        http_server.bind(self.settings.get('port',8080))
        http_server.start(self.settings.get('num_processes', 1))
        loop = tornado.ioloop.IOLoop.instance()
        loop.start()
