#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from kids import Kids


class KidsHandler(logging.Handler):
    def __init__(self, topic=None, host='localhost', port=6379, **kwargs):
        logging.Handler.__init__(self)
        self._topic = topic
        self._kids = Kids(host, port, **kwargs)

    def emit(self, record):
        """
        get topic name from first not None of follow
        1. topic from extra param
        2. handler's topic
        3. logger's name
        4. module name prefixed by 'zhihu.'
        """
        topic = record.topic if hasattr(record, 'topic') else None
        topic = topic or self._topic or 'zhihu.' + record.module
        try:
            self._kids.publish(topic, self.format(record))
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)
