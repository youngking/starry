#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import traceback

def get_traceback_info():
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback_info = None
    traceback_info = traceback.format_exception(exc_type, exc_value, exc_traceback)
    return traceback_info 
