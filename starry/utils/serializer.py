# -*- coding: utf-8 -*-

'''
Serializing messages exchanged by server and client
'''
__all__ = ['Serializer']

import json
import datetime
import decimal

class Serializer(object):
    # Supported serialization format
    SUPPORTED_FORMATS = ['YAML', 'JSON','PROTOBUF']
    
    def __init__(self, format = 'JSON'):
        '''
        创建一个序列化处理器。
        
        :param format: 指定该序列化处理器采用的格式，如 YAML、JSON 等。
        :type format: str
        '''
        format = format.upper()
        if format in self.SUPPORTED_FORMATS:
            self.format = format
        else:
            raise ValueError('unsupported serializaion format')

    
    def load(self, stream):
        '''
        从参数 ``stream`` 中获取数据。
        
        :param stream: 要载入数据的来源。可以是字符串或文件等类型。
        :type stream:mixed
        :rtype stream: str|unicode|file
        '''
        func_name = ''.join(['_from_', self.format.lower()])
        func = globals()[func_name]
        return func(stream)

    
    def dump(self, data):
        '''
        将指定数据 ``data`` 转换为序列化后的信息。
        
        :param data: 要序列化的数据。通常是某种映射或序列类型。
        :type data: mixed
        :rtype: str|unicode
        '''
        func_name = ''.join(['_to_', self.format.lower()])
        func = globals()[func_name]
        return func(data)

    
    def serialize(self, data):
        '''
        :meth: alias of `dump`
        '''
        return self.dump(data)


    def unserialize(self, stream):
        '''
        :meth: alias of `load`
        '''
        return self.load(stream)


def _from_yaml(stream):
    '''Load data from a YAML file or string.'''
    from yaml import load
    try:
        from yaml import CLoader as Loader
    except ImportError:
        from yaml import Loader
    data = load(stream, Loader=Loader)
    return data


def _to_yaml(data):
    '''Dump data into a YAML string.'''
    from yaml import dump
    try:
        from yaml import CDumper as Dumper
    except ImportError:
        from yaml import Dumper
    return dump(data, Dumper=Dumper)


def _from_json(stream):
    '''Load data form a JSON file or string.'''
    if isinstance(stream, basestring):
        data = json.loads(stream)
    else:
        data = json.load(stream)
    return data


def _to_json(data):
    '''Dump data into a JSON string.'''
    return json.dumps(data, cls=AwareJSONEncoder)


    
class AwareJSONEncoder(json.JSONEncoder):
    '''
    JSONEncoder subclass that knows how to encode date/time and decimal types,
    and also ResultProxy/RowProxy of SQLAlchemy .
    '''

    # TODO: timezone support
    DATE_FORMAT = '%Y-%m-%d'
    TIME_FORMAT = '%H:%M:%S'

    def default(self, o):
        from sqlalchemy.engine.base import ResultProxy, RowProxy
        if isinstance(o, datetime.datetime):
            return o.strftime('{0} {1}'.format(self.DATE_FORMAT, self.TIME_FORMAT))
        elif isinstance(o, datetime.date):
            return o.strftime(self.DATE_FORMAT)
        elif isinstance(o, datetime.time):
            return o.strftime(self.TIME_FORMAT)
        elif isinstance(o, decimal.Decimal):
            return str(o)
        elif isinstance(o, ResultProxy):
            return list(o)
        elif isinstance(o, RowProxy):
            return dict(o)
        else:
            return super(AwareJSONEncoder, self).default(o)
