#!/usr/bin/env python
import unittest
from flexmock import flexmock

from protorpc import messages
from protorpc import remote
from protorpc import transport


class Request(messages.Message):
    """Test request message."""
    value = messages.StringField(1)


class Response(messages.Message):
    """Test response message."""
    value = messages.StringField(1)


class SimpleResponse(messages.Message):
    """Simple response message type used for tests."""


class SomeService(remote.Service):

    @remote.method(Request, Response)
    def one_method(self, request):
        response = Response()
        response.value = request.value
        return response

    @remote.method(Request, SimpleResponse)
    def two_method(self, request):
        response = SimpleResponse()
        return response


class InterfaceTest(unittest.TestCase):

    def test_method(self):
        self.assertEquals(Request, \
                SomeService.one_method.remote.request_type)
        self.assertEquals(Response, \
                SomeService.one_method.remote.response_type)

    def test_message(self):
        request = Request()
        service = SomeService()
        self.assertEquals(SimpleResponse(), service.two_method(request))


class ClientTest(unittest.TestCase):

    def setUp(self):
        self.transport = flexmock()

    def test_remote_methods(self):
        stub = SomeService.Stub(self.transport)
        self.assertEquals(stub.all_remote_methods(), \
                SomeService.all_remote_methods())

    def test_with_request(self):
        stub = SomeService.Stub(self.transport)
        request = Request()
        request.value = "hello"
        response = Response()

        rpc = transport.Rpc(request)
        rpc.set_response(response)
        self.transport.should_receive('send_rpc').with_args(
                SomeService.one_method.remote, request).and_return(rpc)

        self.assertEquals(Response(), stub.one_method(request))
        self.assertEquals(Response(), stub.one_method(value="hello"))
